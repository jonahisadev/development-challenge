# Final Thoughts

I wanted to quickly write-up my experience with this test.

## Challenge 1

This first challenge was quite easy once I figured out what was actually happening.
It took me a little bit to actually find out what was wrong. I probably could've sped
that up by reading ahead to the second challenge since there's a small hint there. Once
I figured it out, the fix was simple

## Challenge 2

This one was a bit more frustrating for me. Not because I'm unfamiliar with testing,
but I'm more familiar with backend testing. Testing React was a new challenge for
me, but I did enjoy getting that experience.

## Challenge 3

This was the most fun, because I got to play around in the backend more, which tends
to be my comfort zone. Express is my bread and butter, so writing some API code was
great. I'm not familiar with Material UI, but thankfully my experience with React
helped me along. I had a lot of fun getting the table to dynamically load data
from the API and getting that to look nice.

## Bonuses

I am unfortunately coming up on the 2-3 hour mark, so I don't have any time during this
test to implement any of the bonuses. I can write about how I would implement the sorting
though, just in case that will help me out any:

* The `GET /users` endpoint needs to take a `sortBy` query parameter.
* Pass sort state up to the application in order to make the API call
* The API should return sorted data upon request, in conjunction with the pagination

Pretty basic, but I think that would do the trick. Sadly, I don't have the time to
implement this.

## Thank you!

Thanks for giving me the opportunity to work on this test! I always have a blast with these
but this one was the most fun.