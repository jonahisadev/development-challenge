import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

export const getUsers = async (page) => {
  const { data } = await axios.get(`${userServiceBaseUrl}/users?page=${page}`);
  return data;
};

export const getUserCount = async () => {
  const { data } = await axios.get(`${userServiceBaseUrl}/users/count`);
  return data;
}